package env

import (
	"fmt"

	"github.com/spf13/viper"
)

func LoadConfig() {

	viper.SetConfigName(".env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetConfigType("env")
	err := viper.ReadInConfig()

	if err != nil {
		fmt.Print("err", err)
	}

	viper.SetDefault("APP_PORT", "8080")
}
