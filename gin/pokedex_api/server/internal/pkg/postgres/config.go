package postgres

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	Name     string
	SSLMode  string
}

func LoadConfig() Config {
	return Config{
		Host:     viper.GetString("POSTGRES_HOST"),
		Port:     viper.GetString("POSTGRES_PORT"),
		Username: viper.GetString("POSTGRES_USER"),
		Password: viper.GetString("POSTGRES_PASS"),
		Name:     viper.GetString("POSTGRES_DB_NAME"),
		SSLMode:  viper.GetString("POSTGRES_SSL"),
	}
}

func genConString(c Config) string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s TimeZone=Asia/Bangkok", c.Host, c.Port, c.Username, c.Password, c.Name, c.SSLMode)
}

func InitGormDb() *gorm.DB {
	cf := LoadConfig()
	connStr := genConString(cf)
	db, err := gorm.Open(postgres.Open(connStr), &gorm.Config{})

	if err != nil {
		log.Fatal("Failed to connect database.")
	}

	return db
}
