CREATE TABLE pokemon_normal_detail(
  poke_number int UNSIGNED PRIMARY KEY,
  poke_name varchar(255),
  generation varchar(255),
  classfication varchar(255),
  abilities varchar(255),
  height_m int UNSIGNED,
  weight_kg int UNSIGNED,
  is_legendary varchar(1),
  is_mythical varchar(1),
  is_mega varchar(1)
)