package data_collector

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
)

type PokedexSchema struct {
}

func ReadPokedexData() {

	f, err := os.Open("../internal/app/data_collector/pokemon_data/Pokemon_data.csv")
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()
	csvReader := csv.NewReader(f)
	// for {
	rec, err := csvReader.Read()
	// if err == io.EOF {
	// 	break
	// }
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%+v\n", len(rec))
	// }
}
