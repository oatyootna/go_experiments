module gitlab.com/oatyootna/go_experiments/gin/pokedex_api/server

go 1.13

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/spf13/viper v1.11.0
	golang.org/x/crypto v0.0.0-20220427172511-eb4f295cb31f // indirect
	gorm.io/driver/postgres v1.3.5
	gorm.io/gorm v1.23.5
)
