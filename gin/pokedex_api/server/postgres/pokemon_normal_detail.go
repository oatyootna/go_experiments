package postgres

import (
	"log"

	"gorm.io/gorm"
)

type PokemonNormalDetail struct {
	PokeNumber    string
	PokeName      string
	Generation    string
	Classfication string
	Abilities     string
	HeightM       float32
	WeightKg      float32
	IsLegendary   bool
	IsMythical    bool
	IsMega        bool
}

type PokedexRepo struct {
	db *gorm.DB
}

func FindPokemonByNo(pn string, db *gorm.DB) {
	var pokemon PokemonNormalDetail
	tx := db.Where("poke_number = ?", pn).First(&pokemon)

	if tx.Error != nil {
		log.Fatal(tx.Error)
	}

	log.Println(tx)

}
