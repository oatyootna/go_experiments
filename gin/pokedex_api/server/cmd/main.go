package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/oatyootna/go_experiments/gin/pokedex_api/server/api/task"
	"gitlab.com/oatyootna/go_experiments/gin/pokedex_api/server/internal/pkg/env"
)

func init() {
	env.LoadConfig()
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/pokemons", task.GetPokemonById)
	return r
}

func main() {

	r := setupRouter()
	r.Run(":8080")

}
