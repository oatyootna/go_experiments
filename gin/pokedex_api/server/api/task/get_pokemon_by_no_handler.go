package task

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/oatyootna/go_experiments/gin/pokedex_api/server/postgres"
)

func GetPokemonById(c *gin.Context) {
	pokemon := postgres.PokemonNormalDetail{
		PokeNumber: "1",
	}

	c.JSON(http.StatusOK, pokemon)
}
