package pokemons

import "errors"

type Pokemon struct {
	Id, Name string
}

var ErrNotFound = errors.New("the pokemon not found.")
