package pokedex

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

type getPokemonRequest struct {
	Id string `json:"id"`
}

type getPokemonResponse struct {
	Id    string `json:"id"`
	Name  string `json:"name,omitempty"`
	Error error  `json:"error,omitempty"`
}

func (r getPokemonResponse) error() error { return r.Error }

func makeGetPokemonEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPokemonRequest)
		poke, err := svc.GetPokemon(req.Id)

		if err != nil {
			return getPokemonResponse{Error: err}, nil
		}

		return getPokemonResponse{poke.Id, poke.Name, err}, nil
	}
}
