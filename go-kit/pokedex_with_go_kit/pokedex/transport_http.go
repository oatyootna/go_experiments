package pokedex

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/oatyootna/go_experiments/go-kit/pokedex_with_go_kit/pokemons"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func MakeHandler(svc Service, logger kitlog.Logger) http.Handler {

	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		kithttp.ServerErrorEncoder(encodeError),
	}

	getPokemonHandler := kithttp.NewServer(makeGetPokemonEndpoint(svc),
		decodeGetPokemonRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/api/v1/pokemons", getPokemonHandler).Methods("POST")
	return r
}

func decodeGetPokemonRequest(_c context.Context, r *http.Request) (interface{}, error) {
	var req getPokemonRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	return req, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, rs interface{}) error {
	if e, ok := rs.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(rs)
}

type errorer interface {
	error() error
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	switch err {
	case pokemons.ErrNotFound:
		w.WriteHeader((http.StatusNotFound))
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
