package pokedex

import (
	"time"

	"github.com/go-kit/kit/metrics"
	"gitlab.com/oatyootna/go_experiments/go-kit/pokedex_with_go_kit/pokemons"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

func NewInstrumentingService(counter metrics.Counter, latency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   counter,
		requestLatency: latency,
		Service:        s,
	}
}

func (s *instrumentingService) GetPokemon(id string) (pokemons.Pokemon, error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "get_pokemon").Add(1)
		s.requestLatency.With("method", "get_pokemon").Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.GetPokemon(id)
}
