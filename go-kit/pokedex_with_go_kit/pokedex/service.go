package pokedex

import (
	"gitlab.com/oatyootna/go_experiments/go-kit/pokedex_with_go_kit/pokemons"
)

type Service interface {
	GetPokemon(id string) (pokemons.Pokemon, error)
}

type service struct{}

func NewService() *service {
	return &service{}
}

func (ps *service) GetPokemon(id string) (pokemons.Pokemon, error) {

	bulbasaur := pokemons.Pokemon{"1", "Bulbasaur"}
	ivysaur := pokemons.Pokemon{"2", "Ivysaur"}
	venusaur := pokemons.Pokemon{"3", "Venusaur"}
	charmander := pokemons.Pokemon{"4", "Charmander"}
	charmeleon := pokemons.Pokemon{"5", "Charmeleon"}
	charizard := pokemons.Pokemon{"6", "Charizard"}

	pokemonArr := []pokemons.Pokemon{
		bulbasaur, ivysaur, venusaur, charmander, charmeleon, charizard}

	for _, pokemon := range pokemonArr {
		if pokemon.Id == id {
			return pokemon, nil
		}
	}

	return pokemons.Pokemon{}, pokemons.ErrNotFound
}
