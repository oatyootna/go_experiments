module gitlab.com/oatyootna/go_experiments/go-kit/pokedex_with_go_kit

go 1.13

require (
	github.com/go-kit/kit v0.12.0
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.12.1
	github.com/prometheus/common v0.34.0 // indirect
	github.com/spf13/viper v1.11.0
	gitlab.com/oatyootna/go_experiments/gin/pokedex_api/server v0.0.0-20220505123251-d95fbedc79e5 // indirect
)
